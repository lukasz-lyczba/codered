* Each of below steps can be done with either SourceTree, Intellij Idea or CMD
* Please don't go to next step without being asked

1. Check out project from git
    CMD git clone https://bitbucket.org/lukasz-lyczba/codered/src

2. Import project to IntelliJ
    add files to .gitignore

3. Create a branch with your name and surname instead of "<branch_name>"

    create the branch
    CMD: git branch <branch_name>

    switch to that branch - this command can switch to any branch
    git checkout <branch_name>

4. Create class GitTraining & Employee in package "com.codered"

    -------------------------------- GitTraining -----------------------------------------
    
    package com.codered;

    public class GitTraining {

        private static final Employee[] employees = new Employee[10];

        public static void main(String [] args) {

        }
    }
    
    --------------------------------- Employee -------------------------------------------
    
    package com.codered;

    public class Employee {

    }
    
    --------------------------------------------------------------------------------------

5. Add two new classes to GIT

    will add specific file to git
    CMD: git add <file name>

    will add all files to git
    CMD: git add *

6. Commit changes to local repository
    CMD: git commit -m "I've added new employee and main class"

7. At this point files are committed but only to your local repository, you still have to PUSH to remote
    CMD: git push origin <branch_name>

8. Someone else edited files in your branch, you want to check what has changed and get them
    CMD: git pull

    - at this point instructor has changed your files to
    
    -------------------------------- GitTraining -----------------------------------------
    
    package com.codered;

    public class GitTraining {

        private static final Employee[] employees = new Employee[3];

        public static void main(String [] args) {
            Employee employee0 = new Employee();
            employee0.setName("Bill");
            employees[0] = employee0;

            Employee employee1 = new Employee();
            employee1.setName("John");
            employees[1] = employee1;

            Employee employee2 = new Employee();
            employee2.setName("Paul");
            employees[2] = employee2;

            for(int i = 0; i < employees.length; i++) {
                System.out.println("We have " + employees[i].getName() + " at index " + i);
            }
        }
    }
    
    --------------------------------- Employee -------------------------------------------
    
    package com.codered;

    public class Employee {

        public String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    
    --------------------------------------------------------------------------------------

9. Now let's resolve some conflicts
    - change "employee0" to "emp0", "employee1" to "emp1" and "employee2" to "emp2"
    - pull from repository like in step 8
    - at this point you should face a conflict because someone else changed "GitTraining"
    
    -------------------------------- GitTraining -----------------------------------------
    
    package com.codered;

    public class GitTraining {

        private static final Employee[] employees = new Employee[3];

        public static void main(String [] args) {
            Employee bill = new Employee();
            bill.setName("Bill");
            employees[0] = bill;

            Employee john = new Employee();
            john.setName("John");
            employees[1] = john;

            Employee paul = new Employee();
            paul.setName("Paul");
            employees[2] = paul;

            for(int index = 0; index < employees.length; index++) {
                System.out.println("We have " + employees[index].getName() + " at index " + index);
            }
        }
    }
    
    --------------------------------------------------------------------------------------